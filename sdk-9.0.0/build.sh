#!/bin/bash -ex

if [ "$1" = "-q" ] || [ "$1" = "--quiet" ]; then
    repoflags=--quiet
else
    repoflags=
fi

~/bin/repo init $repoflags -u https://android.googlesource.com/platform/manifest -b android-9.0.0_r18
~/bin/repo sync $repoflags --current-branch -j4

# Still needed?
export USER=$(whoami)

# https://source.android.com/setup/build/building#initialize
. build/envsetup.sh
# https://source.android.com/setup/build/building#choose-a-target
lunch sdk-user
# https://source.android.com/setup/build/building#build-the-code
make sdk -j$(nproc) showcommands
make win_sdk -j$(nproc) showcommands

ls -lh \
   out/host/linux-x86/sdk/sdk/android-sdk_eng.android_linux-x86.zip \
   out/host/windows/sdk/sdk/android-sdk_eng.android_windows.zip

# useful?
# out/target/product/generic/sdk-symbols-eng.android.zip
