#!/bin/bash -ex

if [ "$1" = "-q" ] || [ "$1" = "--quiet" ]; then
    repoflags=--quiet
else
    repoflags=
fi

~/bin/repo init $repoflags -u https://android.googlesource.com/platform/manifest -b android-6.0.1_r31
~/bin/repo sync $repoflags

# out/host/linux-x86/bin/jack-admin: line 27: USER: unbound variable
export USER=$(whoami)

. build/envsetup.sh
lunch sdk-eng
make sdk -j$(nproc) showcommands
make win_sdk -j$(nproc) showcommands
ls -lh \
   out/host/linux-x86/sdk/sdk/android-sdk_eng.android_linux-x86.zip \
   out/host/windows/sdk/sdk/android-sdk_eng.android_windows.zip
