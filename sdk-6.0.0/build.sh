#!/bin/bash -ex

if [ "$1" = "-q" ] || [ "$1" = "--quiet" ]; then
    repoflags=--quiet
else
    repoflags=
fi

~/bin/repo init $repoflags -u https://android.googlesource.com/platform/manifest -b android-6.0.0_r1
~/bin/repo sync $repoflags

export USER=$(whoami)
. build/envsetup.sh
lunch sdk-eng
make sdk -j$(nproc) showcommands
make win_sdk -j$(nproc) showcommands
