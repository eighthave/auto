#!/bin/bash -ex

# Configuration
#echo 'APT::Install-Recommends "false";' > /etc/apt/apt.conf.d/00InstallRecommends

# clean-up non-free sources
# AFAICS we need to update the base image as of 2018-11-22 as it's already partially upgraded
cat <<EOF > /etc/apt/sources.list
deb http://archive.ubuntu.com/ubuntu/ trusty main universe
deb http://archive.ubuntu.com/ubuntu/ trusty-updates main universe
#deb http://security.ubuntu.com/ubuntu/ trusty-security main universe
#deb http://archive.ubuntu.com/ubuntu/ trusty-backports main universe
EOF
dpkg --add-architecture i386
apt-get update

#echo 'dash dash/sh boolean false' | debconf-set-selections
#DEBIAN_FRONTEND=noninteractive dpkg-reconfigure --pri=high dash



export DEBIAN_FRONTEND=noninteractive

# https://android.googlesource.com/platform/ndk/+/master/infra/docker/Dockerfile
apt-get -y install \
  bison build-essential curl dos2unix flex git make pbzip2 python python-pip \
  texinfo uuid-runtime zip
pip install setuptools
