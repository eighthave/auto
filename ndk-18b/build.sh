#!/bin/bash -ex

# https://android.googlesource.com/platform/ndk/+/master/README.md

if [ "$1" = "-q" ] || [ "$1" = "--quiet" ]; then
    repoflags=--quiet
else
    repoflags=
fi

# Preparing build tree
# Make 'repo' accessible to the build system later
export PATH=~/bin:$PATH
repo init $repoflags -u https://android.googlesource.com/platform/manifest -b ndk-r18b
repo sync $repoflags --current-branch -j4

# Build NDK - proper build
(
    cd ndk/
    python checkbuild.py --no-build-tests
)

ls -lh out/dist/android-ndk-0-linux-x86_64.tar.bz2
